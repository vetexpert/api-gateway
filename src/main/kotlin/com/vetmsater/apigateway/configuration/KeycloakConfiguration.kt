package com.vetmsater.apigateway.configuration

import org.keycloak.adapters.springboot.KeycloakSpringBootProperties
import org.keycloak.admin.client.Keycloak
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class KeycloakConfiguration(
    private val properties: KeycloakSpringBootProperties
) {

    @Value("\${keycloak-custom.username}")
    private lateinit var username: String

    @Value("\${keycloak-custom.password}")
    private lateinit var password: String

    @Bean
    fun keycloak(): Keycloak = Keycloak.getInstance(
        properties.authServerUrl,
        properties.realm,
        username,
        password,
        properties.resource,
        properties.clientKeyPassword
    )

}
