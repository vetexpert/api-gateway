package com.vetmsater.apigateway.client

import com.vetexpert.proto.visit.VisitModel
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.util.DefaultUriBuilderFactory
import reactor.core.publisher.Mono
import java.util.*

@Component
class VisitClient(
    webClientProperties: WebClientProperties
) {
    private val webClient = WebClient.builder()
        .uriBuilderFactory(DefaultUriBuilderFactory(webClientProperties.visitUrl)).build()

    fun getAll(filter: VisitModel.VisitFilter, clinicIds: List<String>) =
        webClient.post()
            .uri("/visit/list/short")
            .bodyValue(filter.toByteArray())
            .header("ClinicIds", clinicIds.toString())
            .retrieve()
            .bodyToMono(VisitModel.VisitList::class.java)

    fun getById(id: UUID): Mono<VisitModel.Visit> =
        webClient.get()
            .uri("/visit/$id")
            .retrieve()
            .bodyToMono(VisitModel.Visit::class.java)


}
