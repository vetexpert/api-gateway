package com.vetmsater.apigateway.client

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("services")
data class WebClientProperties(
    val visitUrl: String,
    val userUrl: String
)
