package com.vetmsater.apigateway.client

import com.vetexpert.proto.user.UserModel
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.util.DefaultUriBuilderFactory
import reactor.core.publisher.Mono

@Component
class UserClient(
    webClientProperties: WebClientProperties
) {
    private val webClient = WebClient.builder()
        .uriBuilderFactory(DefaultUriBuilderFactory(webClientProperties.userUrl))
        .build()

    fun getDoctors(userId: String) =
        webClient.get()
            .uri("/user/doctors?userId=$userId")
            .retrieve()
            .bodyToMono(UserModel.UserList::class.java)

    fun getGroupIdsByUserId(userId: String): Mono<List<*>> =
        webClient.get()
            .uri("/group/byUserId/$userId")
            .retrieve()
            .bodyToMono(List::class.java)
}
