package com.vetmsater.apigateway.web.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.protobuf.ProtobufJsonFormatHttpMessageConverter

@Configuration
class ProtobufHttpMessageConverterConfig {

    @Bean
    fun protobufHttpMessageConverter(): ProtobufJsonFormatHttpMessageConverter =
        ProtobufJsonFormatHttpMessageConverter()
}
