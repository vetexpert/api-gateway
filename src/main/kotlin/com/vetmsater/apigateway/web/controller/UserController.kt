package com.vetmsater.apigateway.web.controller

import com.vetexpert.proto.user.UserModel
import com.vetmsater.apigateway.service.UserService
import com.vetmsater.apigateway.web.utils.principalInfo
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class UserController(
    private val userService: UserService
) {

    val logger = LoggerFactory.getLogger(this::class.java)

    @GetMapping(path = ["/user/current"])
    fun getUserInfo(): Mono<UserModel.User> = Mono.fromCallable {
        principalInfo().let {
            UserModel.User.newBuilder().apply {
                id = it.subject
                fio = "${it.familyName} ${it.givenName} ${it.middleName}"
                avatar = it.picture
                position = it.profile
                phoneNumber = it.otherClaims.getOrDefault("phoneNumber", "") as String
            }
                .addAllRole(it.realmAccess.roles)
                .build()
        }
    }

    @GetMapping(path = ["/user/doctors"])
    fun getDoctors(): Mono<UserModel.UserList> = userService.getDoctors(principalInfo().subject)

}
