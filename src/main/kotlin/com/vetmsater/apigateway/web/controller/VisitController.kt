package com.vetmsater.apigateway.web.controller

import com.vetexpert.proto.visit.VisitModel
import com.vetmsater.apigateway.service.VisitService
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class VisitController(
    private val visitService: VisitService
) {
    @RequestMapping(path = ["/visit/all"], method = [RequestMethod.POST])
    fun getAll(@RequestBody visitFilter: VisitModel.VisitFilter) =
        visitService.getAll(visitFilter)

    @RequestMapping(path = ["/visit/{id}"], method = [RequestMethod.GET])
    fun getById(@PathVariable id: UUID) =
        visitService.getById(id)
}
