package com.vetmsater.apigateway.web.filter

import com.vetmsater.apigateway.web.utils.principalInfo
import org.keycloak.KeycloakPrincipal
import org.keycloak.KeycloakSecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class LoggerFilter : OncePerRequestFilter() {
    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        principalInfo().also {
                logger.info(
                    "Request by [${it.preferredUsername}]: [${request.method} ${request.requestURI}] "
                )
            }
        filterChain.doFilter(request, response)
    }
}
