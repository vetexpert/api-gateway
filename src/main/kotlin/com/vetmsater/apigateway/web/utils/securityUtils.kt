package com.vetmsater.apigateway.web.utils

import org.keycloak.KeycloakPrincipal
import org.keycloak.KeycloakSecurityContext
import org.keycloak.representations.AccessToken
import org.springframework.security.core.context.SecurityContextHolder

fun principalInfo(): AccessToken = (SecurityContextHolder.getContext().authentication.principal
        as KeycloakPrincipal<KeycloakSecurityContext>)
    .keycloakSecurityContext.token
