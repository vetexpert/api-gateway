package com.vetmsater.apigateway.web.model

data class UserDTO(
    val id: String? = null,
    val fio: String? = null,
    val avatar: String? = null,
    val position: String? = null,
    val phoneNumber: String? = null,
    val roles: Set<String>? = null
)
