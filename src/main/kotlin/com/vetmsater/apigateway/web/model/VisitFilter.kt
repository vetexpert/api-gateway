package com.vetmsater.apigateway.web.model

import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

data class VisitFilter(
    val state: List<String> = emptyList(),
    val fio: String = "",
    val doctorIds: List<UUID> = emptyList(),
    val phone: String = "",
    val diagnosis: String = "",
    val petName: String = "",
    val dateFrom: LocalDate = LocalDate.now(),
    val dateTo: LocalDate = LocalDate.now().plusDays(1),
)
