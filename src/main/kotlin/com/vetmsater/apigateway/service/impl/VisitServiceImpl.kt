package com.vetmsater.apigateway.service.impl

import com.vetexpert.proto.visit.VisitModel
import com.vetmsater.apigateway.client.UserClient
import com.vetmsater.apigateway.client.VisitClient
import com.vetmsater.apigateway.service.VisitService
import com.vetmsater.apigateway.web.utils.principalInfo
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.util.*

@Service
class VisitServiceImpl(
    private val visitClient: VisitClient,
    private val userClient: UserClient
) : VisitService {

    override fun getAll(filter: VisitModel.VisitFilter) =
        userClient.getGroupIdsByUserId(principalInfo().subject)
            .flatMap { groupIds ->
                visitClient.getAll(filter, groupIds as List<String>)
            }

    override fun getById(id: UUID): Mono<VisitModel.Visit> =
        visitClient.getById(id)

}
