package com.vetmsater.apigateway.service.impl

import com.vetexpert.proto.user.UserModel
import com.vetmsater.apigateway.client.UserClient
import com.vetmsater.apigateway.service.UserService
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class UserServiceImpl(private val userClient: UserClient) : UserService {


    override fun getDoctors(userId: String): Mono<UserModel.UserList> =
        userClient.getDoctors(userId)
}
