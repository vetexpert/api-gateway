package com.vetmsater.apigateway.service

import com.vetexpert.proto.visit.VisitModel
import reactor.core.publisher.Mono
import java.util.*

interface VisitService {

    fun getAll(filter: VisitModel.VisitFilter): Mono<VisitModel.VisitList>
    fun getById(id: UUID): Mono<VisitModel.Visit>

}
