package com.vetmsater.apigateway.service

import com.vetexpert.proto.user.UserModel
import reactor.core.publisher.Mono

interface UserService {
    fun getDoctors(userId: String): Mono<UserModel.UserList>

}
