FROM openjdk:11.0.10-jre-slim
COPY build/libs/api-gateway-*.jar /home/app/api-gateway.jar
ENTRYPOINT ["java", "-jar", "/home/app/api-gateway.jar"]
